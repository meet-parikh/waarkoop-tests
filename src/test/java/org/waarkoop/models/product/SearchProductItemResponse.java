package org.waarkoop.models.product;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SearchProductItemResponse {

    @JsonProperty("provider")
    private String provider;

    @JsonProperty("title")
    private String title;

    @JsonProperty("url")
    private String url;

    @JsonProperty("brand")
    private String brand;

    @JsonProperty("price")
    private Double price;

    @JsonProperty("unit")
    private String unit;

    @JsonProperty("isPromo")
    private Boolean isPromo;

    @JsonProperty("promoDetails")
    private String promoDetails;

    @JsonProperty("image")
    private String image;

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Boolean getPromo() {
        return isPromo;
    }

    public void setPromo(Boolean promo) {
        isPromo = promo;
    }

    public String getPromoDetails() {
        return promoDetails;
    }

    public void setPromoDetails(String promoDetails) {
        this.promoDetails = promoDetails;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
