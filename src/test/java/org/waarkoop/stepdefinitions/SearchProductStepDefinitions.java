package org.waarkoop.stepdefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import org.waarkoop.models.product.SearchProductItemResponse;
import org.waarkoop.services.ProductService;
import org.waarkoop.validators.ResponseStatusValidator;

import java.util.List;

import static net.serenitybdd.rest.SerenityRest.lastResponse;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class SearchProductStepDefinitions {

    @Steps
    private ResponseStatusValidator responseStatusValidator;

    @Steps
    private ProductService productService;

    @When("I search for {string}")
    public void iSearchForProduct(String product) {
        productService.searchProduct(product);
    }

    @Then("the search response code should be {int}")
    public void theSearchResponseCodeShouldBe(int statusCode) {
        responseStatusValidator.validate(statusCode);
    }

    @And("I validate that required fields are present in the response")
    public void iValidateThatRequiredFieldsArePresentInTheResponse() {
        List<SearchProductItemResponse> products = productService.getProductItems();

        products.forEach(product -> {
            assertThat(product.getProvider(), notNullValue());
            assertThat(product.getTitle(), notNullValue());
            assertThat(product.getPrice(), notNullValue());
            assertThat(product.getImage(), notNullValue());
            assertThat(product.getUrl(), notNullValue());
        });
    }

    @Then("I also validate that correct search results are displayed for {string}")
    public void iAlsoValidateThatSearchResultsAreDisplayedFor(String product) {
        List<SearchProductItemResponse> products = productService.getProductItems();

        products.stream().map(SearchProductItemResponse::getTitle).forEach(title -> {
            assertThat(title, containsStringIgnoringCase(product));
        });
    }

    @Then("I should get an error message as {string} for {string}")
    public void iShouldGetAnErrorMessageAs(String error, String product) {
        lastResponse().then()
                .body("detail.error", is(true))
                .body("detail.message", equalTo(error))
                .body("detail.requested_item", equalTo(product));
    }
}
