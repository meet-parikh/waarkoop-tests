package org.waarkoop.validators;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;

public class ResponseStatusValidator {
    public void validate(int code) {
        restAssuredThat(response -> response.statusCode(code));
    }
}
