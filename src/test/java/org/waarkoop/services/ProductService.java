package org.waarkoop.services;

import io.restassured.common.mapper.TypeRef;
import io.restassured.http.ContentType;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.util.EnvironmentVariables;
import org.waarkoop.models.product.SearchProductItemResponse;

import java.util.List;

import static net.serenitybdd.core.environment.EnvironmentSpecificConfiguration.from;
import static net.serenitybdd.rest.SerenityRest.lastResponse;

public class ProductService {

    private static final String SEARCH_PRODUCT_URI = "/api/v1/search/demo/{product}";
    EnvironmentVariables environmentVariables;

    private String getBaseURL() {
        return from(environmentVariables).getProperty("restapi.baseurl");
    }

    public void searchProduct(String product) {
        String url = getBaseURL() + SEARCH_PRODUCT_URI;
        SerenityRest.given()
                .pathParams("product", product)
                .contentType(ContentType.JSON)
                .get(url);
    }

    public List<SearchProductItemResponse> getProductItems() {
        return lastResponse().as(new TypeRef<List<SearchProductItemResponse>>() {
        });
    }

}
