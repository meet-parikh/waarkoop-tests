@search @assignment @product
Feature: Search for the product

### Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/demo/{product} for getting the products.
### Available products: "orange", "apple", "pasta", "cola"
### Prepare Positive and negative scenarios

  Scenario Outline: I search for the product and then I validate the results
    When I search for "<product>"
    Then the search response code should be 200
    And I validate that required fields are present in the response
    Then I also validate that correct search results are displayed for "<product>"
    Examples:
      | product |
      | orange  |
      | apple   |
      | pasta   |
      | cola    |

  Scenario Outline: I search for the invalid product and then I validate error message
    When I search for "<product>"
    Then the search response code should be 404
    Then I should get an error message as "Not found" for "<product>"
    Examples:
      | product |
      | car     |
      | milk    |
      | honey   |
      | beer    |
