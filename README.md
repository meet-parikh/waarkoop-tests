# Project Title

This is an Waarkoop Automation Project using Serenity and Cucumber.

### Built With

* [Serenity](https://serenity-bdd.info/) - Serenity BDD is an open source library for test
  automation.
* [Java](https://www.oracle.com/java/) - Programming Language
* [Maven](https://maven.apache.org/) - Dependency Management

### Checking out and running test

The project gives you the project structure that has automation tests for product search API and supporting
classes.

#### Checkout project using below command

    git checkout https://gitlab.com/meet-parikh/waarkoop-tests.git

#### Go to project root folder

    cd waarkoop-tests

### Local Test Execution

To run the test, you can either just run the `TestRunner` test runner class, or run `mvn clean verify` from the command
line.

### Accessing Local Serenity Report

Serenity Report can be found in `target/site/serenity` directory inside project root directory.
Please open index.html

### Accessing Reports from Gitlab CI

- Go to https://gitlab.com/meet-parikh/waarkoop-tests
- Navigate CI/CD > Pipelines > Click on pipeline number > Click Jobs tab > Click Report under Report stage > Click Browse
  button under Job artifacts

![Screenshot](screenshots/report_runner.png)

- Go into public directory and then click file index.html and the report will be displayed. You may be warned about redirection to an external URL. Please click on the external URL for viewing report.

![Screenshot](screenshots/serenity_report.png)

Alternatively, you can also access the report by getting the job id of report stage and going to below url. Please replace _job-id_ in following URL<br/>
https://meet-parikh.gitlab.io/-/waarkoop-tests/-/jobs/job-id/artifacts/public/index.html

### Environment-specific configurations

We can configure environment specific properties and options in [serenity.conf](https://gitlab.com/meet-parikh/waarkoop-tests/-/blob/main/src/test/resources/serenity.conf), so that the same tests can be run in different
environments. Currently, We have configured three environments, _dev_, _staging_ and _prod_.

Please use the `environment` system property to determine which environment to run against. For example to run the tests in
the staging environment, you could run:

```json
$ mvn clean verify -Denvironment=staging
```

### Directory structure

The project uses Maven as build tool, and follows the standard directory structure used in most Serenity projects:

```Gherkin
src
  + main
  + test
    + java                         Test runners and supporting code
    + resources
      + features                   Feature files
        + search                   Feature file subdirectories
          search_product.feature
```

### Steps to Automate New Test Cases

Please follow existing folder structure to add new tests, In order to automate new endpoint below steps can be followed.

- Create Feature file and add respective test scenarios in it e.g search_product.feature
- Create Step implementation for specific endpoint and response validation under package
  src/test/java/org/waarkoop/stepdefinitions e.g. SearchProductStepDefinitions.java
- Add environment specific new properties if required.

### Project refactoring

1. Clean up project
    1. Removed duplicate build system.
   2. Removed unused dependencies from pom.xml
2. Renamed project structure to be more project specific
3. Refactored Feature file : 
   1. Made feature file more readable
   2. Generic step implementation by parameterizing product name
   3. Added response validation steps
   4. Tagging feature file for test classification.
4. Added separate validator and service class - Easy to maintain the code and reduces duplication
5. Externalized properties

### Tasks

- [x] Automate the endpoints inside a working CI/CD pipeline to test the service • Cleanup the project and implement it in the proper way
- [x] Cover at least 1 positive and 1 negative scenario
- [x] Write instructions in Readme file on how to install, run and write new tests
- [x] Briefly write in Readme what was refactored and why
- [x] Upload your project to Gitlab and make sure that CI/CD is configured
- [x] Set Html reporting tool with the test results of the pipeline test run
- [x] IMPORTANT!!! Provide us with a Gitlab link to your repository